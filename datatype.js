// in js there are 1.primitive and 2.objects are there 
// primitive or the fundamental datatypes 
// 1.null
// 2.Number
// 3.string
// 4.symbol
// 5 BigInt
// 6.Boolean
// 7.undefined

let a= null;
let b= 34;
let c= "piku";
let d= Symbol("I am a nice symbol");
let e= BigInt("356");
let f= true;
let g=undefined;

console.log(a,b,c,d,e,f,g);  
//null 34 piku Symbol(I am a nice symbol) 356n true undefined
console.log("\n\n");
console.log(typeof a);  //object
console.log(typeof b);  //number
console.log(typeof c);  //string
console.log(typeof d);  //symbol
console.log(typeof e);  //BigInt
console.log(typeof f);  //Boolean
console.log(typeof g);  //undefined

console.log("\n\n");

// if we uninitialized a variable that's implicitly assigned undefined
let p;
console.log(p);         //undefined
console.log(typeof p);  //undefined

//but for null you need to explicitly decleared tht it holds nothings

console.log("\n\n");

//Objects are nono-primtitive datatype

// =>I t is accumulation of some properties which has 
// a key-value pair with it.

let alien1 = {};  //oblject
console.log(typeof alien1);

// whatever it is not primitive is an object. 

console.log("\n\n");

let alien2 = {
    name:'Piku',  //property->name with assigned value 'piku'
    tech:'js',    //property->tech with assigned value 'js
}

console.log(alien2);  //{ name: 'Piku', tech: 'js' }
console.log(alien2.name);    //piku  
console.log(alien2['name']); //piku