// 1.create a variable of tye string and try to add a number to in it.
let a ="piku";
let b=23;
let c=a+b;
console.log(c);   //piku23
// 2.concatenation of javascript
console.log("\n\n");
console.log(typeof(c));   //string

console.log("\n\n");

// 3.Create a const object in js. Can you change it to hold a number later?

const d= {
    name:"piku",
    section: 1,
    isPrincipal: true
}

// d= "piku";   //No,it shows you error.

// 4. can you add a new key with this above const object 

d['friend'] = "subham";

// ye you can easily do it. 
console.log(d);
// { name: 'piku', section: 1, isPrincipal: true, friend: 'subham' }

// => Here acctually 'd' is an reference of the above Object. so we cannot reassign,redecleared instanceof.
// But easily we can manipulate the key-value pair data in instanceof.
// we can easily add a property and assign a data to it.