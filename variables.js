// Js is a dynamically typed language which allows us to change the variable name during runtime. 
// That means we can easily put a string value inside a number during rumtime 
// variables 
// Using var
// Using let (most using this)
// Using const
// Using nothing

let a= 56;
console.log(a);  //56

a="piku";  //identifier (variable name) (assignment operator) (assign variable value)
console.log(a); //piku
// above type is a example of dynamically typed language
console.log("\n\n");

//difference between var,let and const:
console.log("For Var");
var b=20;
{
    var b="pinky";
    console.log(b);  //pinky
}
console.log(b);     //pinky
//for var outside of the scope it still grab the value which is initialize inside scope..lost global variable
console.log("\n\n")
console.log("For Let");
let c=29;
{
    let c="papa";
    console.log(c);  //papa
}
console.log(c);   //29
//let grab the global value outside the scope
console.log("\n\n");
var p=30;
console.log(p);
var p=80;
console.log(p);
// var can be reinitialized
console.log("\n\n");
let q=45;
console.log(q);
// let q=39;   //this will show error because let cannot be reinitialize it only can updated
q=39;
console.log(q);


console.log("\n\n");
const pi=3.14;    //it will be constant for all over the code
console.log(pi);
// pi=90; shows error because it cannot be reassigned
{
    // pi=20 it aloso cannaot be reassigned inside a scope
    console.log(pi);
}


